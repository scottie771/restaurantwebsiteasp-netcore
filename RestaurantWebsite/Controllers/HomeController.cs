﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestaurantWebsite.Models;

namespace RestaurantWebsite.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        public IActionResult SendReservationInfo(string name, string date, string time, string numberOfPersons, string phone)
        {
            string reservationDetails = $"New reservation from: {name}, <br>Date: {date}, <br>Time: { time}, <br>Persons: { numberOfPersons},<br>Telephone: {phone}";
            GMailer.GmailUsername = "innovation.outofrange@gmail.com";
            GMailer.GmailPassword = "#S3cr3tPas5w0rd";

            GMailer mailer = new GMailer();
            mailer.ToEmail = "scottie771@gmail.com";
            mailer.Subject = "New Reservation";
            mailer.Body = reservationDetails;
            mailer.IsHtml = true;
            mailer.Send();

           
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
