﻿using RestaurantWebsite.AppContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantWebsite.Models
{
    public class Availability
    {
        //TODO check the number of tables each reservation needs according to the number of persons 

        List<Reservation> reservations = new List<Reservation>();

        public bool GetAvailability(Reservation newReservation)
        {
            List<Reservation> currentReservations = new List<Reservation>();
            using (var db = new DBContext())
            {
                currentReservations = db.Reservations.Where(x => x.ReservationDateTime < newReservation.ReservationDateTime && x.DepartureDateTime > newReservation.ReservationDateTime).ToList();
            }
            int numberOfTables = 100 - currentReservations.Count();
            if (numberOfTables > 0) { return true; }
            else { return false; }
        }

        public bool MakeReservation(Reservation newReservation)
        {
            if (GetAvailability(newReservation))
            {
                try
                {
                    using (var db = new DBContext())
                    {
                        reservations = db.Reservations.ToList();
                        reservations.Add(newReservation);
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else { return false; }
        }
    }
}
