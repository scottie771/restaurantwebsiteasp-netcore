﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantWebsite.Models
{
    public class Reservation
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public int NumberOfPersons { get; set; }
        public DateTime ReservationDateTime { get; set; }
        public DateTime DepartureDateTime { get; set; }

        public Reservation(string name, string phone, int persons, DateTime reservation)
        {
            Name = name;
            Phone = phone;
            NumberOfPersons = persons;
            ReservationDateTime = reservation;
            DepartureDateTime = reservation.AddHours(2);
        }
    }
}
